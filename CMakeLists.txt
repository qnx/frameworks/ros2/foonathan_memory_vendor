cmake_minimum_required(VERSION 3.5)

project(foonathan_memory_vendor)

find_package(ament_cmake REQUIRED)

set(PACKAGE_VERSION "1.0.0")

macro(build_foonathan_memory)

  set(extra_cmake_args)
  
  if(DEFINED CMAKE_BUILD_TYPE)
    list(APPEND extra_cmake_args -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE})
  endif()
  
  list( APPEND extra_cmake_args  -DFOONATHAN_MEMORY_BUILD_EXAMPLES=OFF )
  list( APPEND extra_cmake_args  -DFOONATHAN_MEMORY_BUILD_TESTS=OFF )
  list( APPEND extra_cmake_args  -DBUILD_SHARED_LIBS=ON )
  list( APPEND extra_cmake_args  -DCMAKE_POSITION_INDEPENDENT_CODE=ON )
  list( APPEND extra_cmake_args  -DFOONATHAN_MEMORY_CONTAINER_NODE_SIZES_IMPL=${CMAKE_CURRENT_SOURCE_DIR}/nto/${CPUVARDIR}/include/container_node_sizes_impl.hpp )
  
  list( APPEND extra_cmake_args  -DCMAKE_INSTALL_INCLUDEDIR=${CMAKE_INSTALL_PREFIX}/include )
  list( APPEND extra_cmake_args  -DCMAKE_INSTALL_BINDIR=${CMAKE_INSTALL_PREFIX}/bin )
  list( APPEND extra_cmake_args  -DCMAKE_INSTALL_LIBDIR=${CMAKE_INSTALL_PREFIX}/lib )
  list( APPEND extra_cmake_args  -DCMAKE_INSTALL_DATADIR=${CMAKE_INSTALL_PREFIX}/share )
  
  list( APPEND extra_cmake_args  -DCMAKE_CXX_COMPILE_FEATURES="cxx_constexpr" )
  
  if(DEFINED CMAKE_TOOLCHAIN_FILE)
    list(APPEND extra_cmake_args "-DCMAKE_TOOLCHAIN_FILE=${CMAKE_TOOLCHAIN_FILE}")
  endif()
  
  include(ExternalProject)
  ExternalProject_Add(foonathan_memory
    GIT_REPOSITORY https://github.com/foonathan/memory.git
    #GIT_TAG bf64e407a98d8482c98687bbfd3379585ec95586
    GIT_TAG bab692e382cd58b30ff15af3664604bbc576eb78
    TIMEOUT 600
    CMAKE_ARGS
    -DCMAKE_INSTALL_PREFIX=${CMAKE_INSTALL_PREFIX}
      ${extra_cmake_args}
      -Wno-dev
  )

endmacro()

find_package(foonathan_memory QUIET)
if(NOT foonathan_memory_FOUND)
  build_foonathan_memory()
endif()

ament_package()
